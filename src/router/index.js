import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  linkActiveClass: "active",
  scrollBehavior: () => {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("@/pages/Home/HomePage")
    },
    {
      path: "/start",
      component: () => import("@/pages/Play/PlayPage")
    },
    {
      path: "/monitoring",
      component: () => import("@/pages/Monitoring/MonitoringPage")
    },
    {
      path: "/news",
      component: () => import("@/pages/News/NewsPage")
    },
    {
      path: "/profile",
      component: () => import("@/pages/Profile/MainProfilePage")
    },
    {
      path: "/profile/:user",
      name: "stats",
      props: true,
      component: () => import("@/pages/Profile/StatsPage")
    },
    {
      path: "/donate",
      component: () => import("@/pages/Donate/DonatePage")
    },
    {
      path: "/quiz",
      component: () => import("@/pages/Quiz/QuizPage")
    },
    {
      path: "*",
      component: () => import("@/pages/NotFound/NotFoundPage")
    }
  ]
});
