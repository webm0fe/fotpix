import app from "./app";
import navigation from "./navigation";

export default {
  app,
  navigation
};
