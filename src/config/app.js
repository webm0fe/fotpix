export default {
  site: {
    name: "Fotpix Roleplay",
    description: "Играй в GTA San Andreas по сети",
    address: "192.168.1.1:7777"
  },
  layout: {
    iconSet: "default"
  },
  ab: {
    variant: "default", // Активный вариант отображения
    variants: [] // Варианты для тестов
  }
};
