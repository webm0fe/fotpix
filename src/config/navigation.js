export default {
  topMenu: [
    {
      name: "Мониторинг",
      route: { name: "/monitoring" }
    },
    {
      name: "Новости",
      route: { name: "/news" }
    }
  ],
  mainMenu: [
    {
      name: "Главная",
      route: { name: "/" }
    },
    {
      name: "Личный кабинет",
      route: { name: "/profile" }
    },
    {
      name: "Донат",
      route: { name: "/donate" }
    },
    {
      name: "Как начать играть",
      route: { name: "/start" }
    }
  ]
};
