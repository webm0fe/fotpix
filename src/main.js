import Vue from "vue";
import router from "./router/";
import store from "./store/";
import UncodesCore from "./components/install";
import App from "./layouts/App";

Vue.config.productionTip = false;

Vue.use(UncodesCore);

global.uncodes = App;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
