export default {
  state: {
    news: [
      {
        picture:
          "https://sun9-2.userapi.com/c854028/v854028380/1892b8/3M-Ry6BOUps.jpg",
        desc: `
          На данный момент ведутся работы над
          изменением систем сервера:<br><br>
          1. Новый интерфейс меню F11. Сюда входит
          новый дизайн меню, новое отображение
          карты, статистика персонажа, настройки.
        `,
        publish: {
          date: "17/11/2019",
          time: "16:45"
        }
      },
      {
        picture:
          "https://sun9-2.userapi.com/c854028/v854028380/1892b8/3M-Ry6BOUps.jpg",
        desc: `
        обновление@fotpix
        <br><br>
        Новые изменения у бандитов 🔥
        `,
        publish: {
          date: "6/11/2019",
          time: "00:45"
        }
      },
      {
        picture:
          "https://sun9-2.userapi.com/c854028/v854028380/1892b8/3M-Ry6BOUps.jpg",
        desc: `
        #обновление@fotpix
        <br><br>
        Пока идет работа над версией Beta 2.3 решили порадовать вас большой оптимизацией игрового мода и небольшими изменениями.
        `,
        publish: {
          date: "27/10/2019",
          time: "18:17"
        }
      },
      {
        picture:
          "https://sun9-2.userapi.com/c854028/v854028380/1892b8/3M-Ry6BOUps.jpg",
        desc: `
        #FOTPIX #Fotpix_NoiseBomb
        <br><br>
        Совсем скоро, пройдет фестиваль NOISE Bomb. Сейчас мы вам расскажем, некоторые подробности, которые вам нужно знать.
        Когда и где, мы сообщим позже.
        `,
        publish: {
          date: "21/10/2019",
          time: "12:29"
        }
      },
      {
        picture:
          "https://sun9-2.userapi.com/c854028/v854028380/1892b8/3M-Ry6BOUps.jpg",
        desc: `
        #информация@fotpix
        <br><br>
        Нами была найдена причина отсутствия сообщений на почту доменов mail.ru и была устранена. Теперь вы можете регистрироваться и привязывать почту...
        `,
        publish: {
          date: "11/10/2019",
          time: "19:43"
        }
      },
      {
        picture:
          "https://sun9-2.userapi.com/c854028/v854028380/1892b8/3M-Ry6BOUps.jpg",
        desc: `
        #анонс@fotpix
        <br><br>
        Готовим некоторые изменения на сервере.
        <br><br>
        Небольшой список:
        - Новые интерьеры автосалонов...
        `,
        publish: {
          date: "1/10/2019",
          time: "21:58"
        }
      }
    ]
  },
  mutations: {},
  actions: {},
  getters: {
    news(state) {
      return state.news;
    }
  }
};
