import Vue from "vue";
import Vuex from "vuex";
import appConfig from "@/config/app";

import user from "./user";
import profile from "./profile";
import news from "./news";

Vue.use(Vuex);

export default () => {
  return new Vuex.Store({
    modules: {
      user,
      profile,
      news
    },
    state: appConfig,
    state: {},
    mutations: {
      SET: (state, { key, value }) => {
        state[key] = value;
      }
    },
    actions: {}
  });
};
