export default {
  state: {
    profile: {
      id: 1,
      image:
        "https://images.cdn.circlesix.co/image/2/250/250/5/uploads/profile/aaa05a24a0e2832f339703f7f9b54460.jpg",
      name: "uncodes",
      rights: {
        id: 4,
        value: "Основатель"
      },
      status: true,
      register: {
        date: "07.12.2019",
        time: "04:30"
      }
    }
    // profile: null
  },
  mutations: {},
  actions: {},
  getters: {
    profile(state) {
      return state.profile;
    }
  }
};
