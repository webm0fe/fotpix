export default {
  state: {
    users: [
      {
        id: 1,
        name: "Victor Mayorov",
        skin: 2,
        level: 27,
        exp: {
          value: 12,
          max: 28
        },
        donate: 2000,
        general: [
          { label: "Пол", value: "Мужской" },
          { label: "Фракция", value: "LS Police" },
          { label: "Ранг", value: "Sheriff" },
          { label: "Наличные", value: 18000 },
          { label: "Сбережения", value: 104857 },
          { label: "Законопослушность", value: 0 }
        ],
        cars: [
          {
            picture: "1",
            name: "Uranus",
            price: 2750000,
            type0: {
              value: 46.1,
              max: 75
            },
            type1: {
              value: 0
            },
            type2: {
              value: 19
            },
            type3: {
              value: 0,
              max: 2
            }
          }
        ],
        inventory: [
          { src: "ammow", count: 48 },
          { src: "spas", amount: "12" },
          { src: "deagle", amount: "7 / 12" },
          { src: "ciggy", count: 19 },
          { src: "card", count: 1 },
          { src: "lic", count: 2 },
          { src: "mask", count: 3 },
          { src: "water", count: 1 },
          { src: "weap", count: 4 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 }
        ],
        skillsGuns: [
          {
            label: "Кольт 45",
            value: 76,
            lic: false
          },
          {
            label: "Пустынный орел",
            value: 25,
            lic: true
          },
          {
            label: "Дробовик",
            value: 2,
            lic: true
          },
          {
            label: "Кольт 45",
            value: 76,
            lic: false
          },
          {
            label: "Пустынный орел",
            value: 25,
            lic: true
          },
          {
            label: "Дробовик",
            value: 2,
            lic: true
          }
        ],
        skillsJobs: [
          {
            level: 12,
            label: "Дальнобойщик",
            min: 472,
            max: 999
          },
          {
            level: 2,
            label: "Таксист",
            min: 15,
            max: 25
          },
          {
            level: 6,
            label: "Водитель автобуса",
            min: 60,
            max: 120
          },
          {
            level: 0,
            label: "Старатель",
            min: 2,
            max: 3
          },
          {
            level: 1,
            label: "Механик",
            min: 1,
            max: 15
          }
        ]
      },
      {
        id: 27,
        name: "Christopher Stark",
        skin: 2,
        level: 1,
        exp: {
          value: 6,
          max: 8
        },
        donate: 0,
        general: [
          { label: "Пол", value: "Мужской" },
          { label: "Фракция", value: "LS Police" },
          { label: "Ранг", value: "Sheriff" },
          { label: "Наличные", value: 18000 },
          { label: "Сбережения", value: 104857 },
          { label: "Законопослушность", value: 0 }
        ],
        cars: [
          {
            picture: "1",
            name: "Uranus",
            price: 2750000,
            type0: {
              value: 28.7,
              max: 48
            },
            type1: {
              value: 3
            },
            type2: {
              value: 8
            },
            type3: {
              value: 2,
              max: 2
            }
          },
          {
            picture: "1",
            name: "Uranus New",
            price: 2750000,
            type0: {
              value: 73,
              max: 75
            },
            type1: {
              value: 12
            },
            type2: {
              value: 84
            },
            type3: {
              value: 1,
              max: 2
            }
          }
        ],
        inventory: [
          { src: "ammow", count: 1 },
          { src: "spas", amount: "3" },
          { src: "ciggy", count: 19 },
          { src: "card", count: 1 },
          { src: "lic", count: 2 },
          { src: "mask", count: 3 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 },
          { src: null, count: 0 }
        ],
        skillsGuns: [
          {
            label: "Кольт 45",
            value: 76,
            lic: false
          },
          {
            label: "Пустынный орел",
            value: 25,
            lic: true
          },
          {
            label: "Дробовик",
            value: 2,
            lic: true
          },
          {
            label: "Кольт 45",
            value: 76,
            lic: false
          },
          {
            label: "Пустынный орел",
            value: 25,
            lic: true
          },
          {
            label: "Дробовик",
            value: 2,
            lic: true
          }
        ],
        skillsJobs: [
          {
            level: 12,
            label: "Дальнобойщик",
            min: 472,
            max: 999
          },
          {
            level: 2,
            label: "Таксист",
            min: 15,
            max: 25
          },
          {
            level: 6,
            label: "Водитель автобуса",
            min: 60,
            max: 120
          },
          {
            level: 0,
            label: "Старатель",
            min: 2,
            max: 3
          },
          {
            level: 1,
            label: "Механик",
            min: 1,
            max: 15
          }
        ]
      }
    ]
  },
  mutations: {},
  actions: {},
  getters: {
    users(state) {
      return state.users;
    }
  }
};
