import VueCompositionApi from "@vue/composition-api";
import { VLazyImagePlugin } from "v-lazy-image";
import Meta from "vue-meta";
import lodash from "lodash";
import VueFlashMessage from "vue-flash-message";
require("vue-flash-message/dist/vue-flash-message.min.css");
import VueLodash from "vue-lodash";
import IconLoader from "./icon-loader/plugin";
import UiComponents from "./ui/index";
import Filters from "./filters";

export default {
  install(Vue /*, options*/) {
    //let { store } = options;

    Vue.use(VueFlashMessage);

    Vue.use(VueCompositionApi);
    Vue.use(Meta, {
      keyName: "metaInfo"
    });
    Vue.use(VLazyImagePlugin);

    Vue.use(VueLodash, lodash);

    // Регистрируем глобальные фильтры
    Object.keys(Filters).forEach(key => {
      Vue.filter(key, Filters[key]);
    });

    Object.keys(UiComponents).forEach(key => {
      Vue.component(key, UiComponents[key]);
    });

    Vue.use(IconLoader, {
      packages: {
        default: {}
      }
    });
  }
};
