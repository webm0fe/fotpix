import differenceInSeconds from "date-fns/differenceInSeconds";

export default {
  created() {
    let nowDate = new Date();
    let expDate = new Date(this.expires);
    let secondsLeft = differenceInSeconds(expDate, nowDate);
    if (secondsLeft > 0) {
      this.secondsLeft = secondsLeft;
      this.startTimer();
    }
  },
  data() {
    return {
      secondsLeft: 0,
      timerId: null,
      timeDeclensions: {
        day: ["день", "дня", "дней"],
        hour: ["час", "часа", "часов"],
        minute: ["минута", "минуты", "минут"],
        second: ["секунда", "секунды", "секунд"]
      }
    };
  },
  computed: {
    timerActive() {
      return this.secondsLeft > 0;
    },
    timeParts() {
      let sec = this.secondsLeft;
      let min = sec / 60;
      var hour = min / 60;
      return {
        day: {
          value: Math.floor(hour / 24),
          max: 100
        },
        hour: {
          value: Math.floor(hour % 24),
          max: 24
        },
        minute: {
          value: Math.floor(min % 60),
          max: 60
        },
        second: {
          value: Math.floor(sec % 60),
          max: 60
        }
      };
    }
  },
  methods: {
    startTimer() {
      this.timerId = setInterval(() => this.processTimer(), 1000);
    },
    processTimer() {
      if (this.secondsLeft > 0) {
        this.secondsLeft--;
      } else {
        clearTimeout(this.timerId);
      }
    }
  }
};
