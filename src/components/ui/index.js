import UiInput from "./form-input/UiInput";
import UiCheckbox from "./form-checkbox/UiCheckbox";
import UiButton from "./buttons/UiButton";
import UiModals from "./modals/UiModals";
import UiRadial from "./radial/UiRadial";

export default {
  UiInput,
  UiButton,
  UiModals,
  UiRadial,
  UiCheckbox
};
