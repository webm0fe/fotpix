import { mapState } from "vuex";
import siteConfig from "@/config/app";

export default {
  data() {
    return {
      navigation: siteConfig.navigation
    };
  },
  computed: {
    ...mapState({
      appConfig: state => state.site,
      contacts: state => state.site.contacts
    })
  },
  methods: {
    navLink(pageCode) {
      return this.navigation.pages[pageCode];
    }
  }
};
