import UiIcon from "./UiIcon";
import { iconScheme } from "@/config/icons";

export default {
  install(Vue, options) {
    if (options && options.packages !== undefined) {
      Object.keys(options.packages).forEach(packageKey => {
        iconScheme.forEach(iconName => {
          Vue.component(
            `ui-icon-${iconName}-${packageKey}`,
            require(`@/assets/svg/${packageKey}-pack/${iconName}.svg`).default
          );
        });
      });
    }
    Vue.component("ui-icon", UiIcon);
  }
};
