export default {
  props: {
    icon: {
      type: String,
      required: true
    }
  },
  render(h) {
    return h(`ui-icon-${this.icon}-default`, {
      class: "inline-icon"
    });
  }
};
