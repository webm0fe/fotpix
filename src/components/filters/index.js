import { numberFormat, priceFormat, moneyFormat, declension } from "../utils";

export default { numberFormat, priceFormat, moneyFormat, declension };
